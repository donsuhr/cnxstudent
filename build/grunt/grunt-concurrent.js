module.exports = function (grunt) {
	grunt.config.set('concurrent', {
		options: {
			limit: 6,
			logConcurrentOutput: true
		},
		dist : {
			tasks: [
				'less',
				'assemble',
				'copy:dist',
				'copy:js',
				'imagemin',
				'svgmin'
			]
		},
		preview: {
			tasks: [
				'watch', 'open'
			]
		}
	});
};