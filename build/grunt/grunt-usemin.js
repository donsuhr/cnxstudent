module.exports = function (grunt) {
	grunt.config.set('usemin', {
		options: {
			assetsDirs: ['<%= paths.tmp %>']
		},
		html: ['<%= paths.tmp %>/{,*/}*.html'],
		css: ['<%= paths.tmp %>/styles/{,*/}*.css']
	});
};