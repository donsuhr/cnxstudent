module.exports = function (grunt) {
	grunt.config.set('watch', {
		livereload: {
			options: {
				livereload: '<%= connect.options.livereload %>'
			},
			files: [
				'<%= paths.dist %>/styles/*.css',
				'<%= paths.tmp %>/styles/*.css',
				'<%= paths.tmp %>/*.html',
				'<%= paths.app %>/scripts/*.js',
				'<%= paths.dist %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
			]
		},
		less: {
			files: ['<%= paths.app %>/styles/{,*/}*.less'],
			tasks: ['less', 'autoprefixer']
		},
		'jshint-app': {
			files: [
				'app/**/*.js', '!app/scripts/vendor/**'
			],
			tasks: ['jshint:app', 'copy:js']
		},
		'jshint-build': {
			files: ['Gruntfile.js', 'build/**/*.js'],
			tasks: ['jshint:build']
		},
		assemble: {
			files: [
				'<%= paths.app %>/templates/**/*.hbs'
			],
			tasks: ['assemble']
		}

	});
};