module.exports = function (grunt) {
	grunt.config.set('less', {
		options: {
			compress: false,
			ieCompat: true,
			sourceMap: true,
			sourceMapBasepath: '<%= paths.app %>',
			sourceMapRootpath: '/'
		},
		main: {
			options: {
				sourceMapFilename: '<%= paths.dist %>/styles/main.css.map',
				sourceMapURL: '/styles/main.css.map'
			},
			files: [
				{'<%= paths.tmp %>/styles/main.css': '<%= paths.app %>/styles/main.less'}

			],

		},
		cnxIcons: {
			options: {
				sourceMapFilename: '<%= paths.dist %>/styles/cnxIcons.css.map',
				sourceMapURL: '/styles/cnxIcons.css.map'
			},
			files: [{'<%= paths.tmp %>/styles/cnxIcons.css': '<%= paths.app %>/styles/cnxIcons.less'}]
		},
		noMaps: {
			options: {
				ssourceMap: false
			},
			files: [
				{
					expand: true,
					cwd: '<%= paths.app %>/styles',
					src: ['**/*.less', '!cnxIcons.less', '!main.less'],
					dest: '<%= paths.tmp %>/styles/',
					ext: '.css'
				}
			]
		}

	});
};