module.exports = function (grunt) {
	grunt.config.set('jshint', {
		options: {
			jshintrc: '.jshintrc'
		},

		app: {
			options: {
				jshintrc: '<%= paths.app %>/.jshintrc'
			},
			src: [
				'<%= paths.app %>/**/*.js', '!<%= paths.app %>/scripts/vendor/**'
			]
		},
		build: {
			options: {
				jshintrc: 'build/.jshintrc'
			},
			src: ['Gruntfile.js', 'build/**/*.js']
		}
	});
};