module.exports = function (grunt) {
	grunt.config.set('processhtml', {
		options: {
			includeBase: '',
			customBlockTypes: []
		},
		dist: {
			files: [
				{
					expand: true,
					cwd: '<%= paths.tmp %>',
					src: ['**/*.html'],
					dest: '<%= paths.tmp %>'
				}
			]
		}
	});
};