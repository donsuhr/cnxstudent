module.exports = function (grunt) {

	grunt.config.set('imagemin', {
		dist: {
			files: [
				{
					expand: true,
					cwd: '<%= paths.app %>/images',
					src: '{,*/}*.{gif,jpeg,jpg,png}',
					dest: '<%= paths.dist %>/images'
				}
			]
		}
	});
	grunt.loadNpmTasks('grunt-contrib-imagemin');

};