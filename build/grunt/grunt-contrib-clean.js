module.exports = function (grunt) {

	grunt.config.set('clean', {
		dist: '<%= paths.dist %>',
		tmp: '<%= paths.tmp %>'
	});

	grunt.loadNpmTasks('grunt-contrib-clean');

};