var serveStatic = require('serve-static');

module.exports = function (grunt) {
	grunt.config.set('connect', {
		options: {
			port: 9000, // change this to '0.0.0.0' to access the server from outside
			hostname: '0.0.0.0',
			livereload: 35729
		},
		livereload: {
			options: {
				open: false, //'http://localhost:9000/index.html',
				middleware: function(connect) {
					return [
                        serveStatic(grunt.config.get('paths').tmp),
						connect().use('/bower_components', serveStatic('./bower_components')),
                        serveStatic(grunt.config.get('paths').dist),
                        serveStatic(grunt.config.get('paths').app),
					];
				}
			}
		}
	});
};