module.exports = function (grunt) {
	grunt.config.set('useminPrepare', {
		options: {
			dest: '<%= paths.dist %>'
		},
		html: '<%= paths.tmp %>/index.html'
	});
};