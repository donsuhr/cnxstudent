module.exports = function (grunt) {
	grunt.config.set('uncss', {
		dist: {

			//stylesheets: ['scrape/Style Library/cmc/css/cmc.master.css'],
			src: ['<%= paths.tmp %>/**/*.html'],
			dest: '<%= paths.tmp %>/uncss.css',
			options: {
				report: 'min', // optional: include to report savings
				htmlroot: '<%= paths.tmp %>',
				csspath: '/concat/',
				ignoreSheets: [ /bower_components/, /fonts.googleapis/]

			}
		}
	});
};

