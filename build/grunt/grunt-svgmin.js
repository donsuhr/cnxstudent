module.exports = function (grunt) {

	grunt.config.set('svgmin', {
		dist: {
			files: [
				{
					expand: true,
					cwd: '<%= paths.app %>/images',
					src: '{,*/}*.svg',
					dest: '<%= paths.dist %>/images'
				}
			]
		}
	});
	grunt.loadNpmTasks('grunt-svgmin');

};