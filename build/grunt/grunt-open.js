module.exports = function (grunt) {
	grunt.config.set('open', {
		// using open instead of livereload open to force wait until livereload server started
		// else 404 on http://0.0.0.0:35729/livereload.js?snipver=1
		delayForConcurrent: {
			path: 'http://<%= connect.options.hostname %>:<%= connect.options.port %>/index.html'
		}
	});
};