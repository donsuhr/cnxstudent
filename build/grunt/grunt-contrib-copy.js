module.exports = function (grunt) {

	grunt.config.set('copy', {
		dist: {
			files: [
				{
					expand: true,
					dot: true,
					cwd: '.',
					dest: '<%= paths.dist %>',
					src: [
						'<%= paths.bower %>/bootstrap/dist/css/bootstrap.min.css',
						'<%= paths.bower %>/bootstrap/dist/js/bootstrap.min.js',
						'<%= paths.bower %>/bootstrap/dist/fonts/**/*',
						'<%= paths.bower %>/d3/d3.min.js',
						'<%= paths.bower %>/jquery/dist/jquery.min.js',
						'<%= paths.bower %>/jquery/dist/jquery.min.map'
					]
				},
				{
					expand: true,
					dot: true,
					cwd: '<%= paths.app %>',
					dest: '<%= paths.dist %>',
					src: [
						'fonts/*.{ttf,woff,eot}'
					]
				}
			]
		},
		js: {
			files: [
				{
					expand: true,
					dot: true,
					cwd: '<%= paths.app %>',
					dest: '<%= paths.tmp %>',
					src: [
						'scripts/*.js'
					]
				}
			]
		}
	});

	grunt.loadNpmTasks('grunt-contrib-copy');

};