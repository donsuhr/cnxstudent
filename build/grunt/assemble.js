module.exports = function (grunt) {
	grunt.config.set('assemble', {
		options: {
			flatten: true,
			//cwd: '<%= config.app %>/templates/pages',
			// cwd is broke here https://github.com/assemble/assemble/issues/411
			layout: '<%= paths.app %>/templates/layouts/default.hbs',
			partials: ['<%= paths.app %>/templates/partials/*.hbs']
		},
		pages: {
			files: {
				'<%= paths.tmp %>/': [
					'<%= paths.app %>/templates/pages/**/*.hbs'
				]
			}
		}

	});
	grunt.loadNpmTasks('assemble');
};