'use strict';
module.exports = function (grunt) {
	require('load-grunt-tasks')(grunt);

	var bowerrc = grunt.file.readJSON('./.bowerrc');
	var bowerDir = bowerrc.directory;

	grunt.initConfig({
		paths: {
			app: 'app',
			dist: 'dist',
			tmp: '.tmp',
			bower: bowerDir
		}
	});
	grunt.loadTasks('build/grunt');
	//grunt.registerTask('dist', ['jshint', 'clean:dist', 'concurrent:dist']);
	grunt.registerTask('preview', [
		'jshint', 'clean:dist', 'concurrent:dist', 'autoprefixer', 'connect:livereload', 'concurrent:preview'
	]);
	grunt.registerTask('default', ['preview']);

	grunt.registerTask('dist',
		[
			'clean', 'concurrent:dist', 'autoprefixer', 'useminPrepare', 'concat', 'cssmin', 'uglify',
			'usemin', 'processhtml', 'htmlmin'
		]);
};
