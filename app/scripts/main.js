(function (window, document, $) {
	'use strict';

	var _scrollTimer;
	//	var remove = function(){
	//		$('.mainNav').closest('.navContainer').removeClass('is-dropping');
	//	};

	var _onScrollHandler = function () {
		var top = $(document).scrollTop();
		var $target = $('.mainNav').closest('.navContainer');
		var passed = top >= 45;

		//if (passed && !$target.hasClass('is-dropping') && !$target.hasClass('active')){
		//$target.addClass('is-dropping');
		//setTimeout(remove, 250);
		//}
		$target.toggleClass('active', passed);
	};

	$(window).on('scroll', function () {
		clearTimeout(_scrollTimer);
		_scrollTimer = setTimeout(_onScrollHandler, 250);
	});

	$(document).ready(function () {
		$('.dropdown').on('show.bs.dropdown', function () {
			var $menu = $(this).find('.dropdown-menu');
			$menu.first().stop(true, true).slideDown('fast');
			$menu.attr('aria-hidden', 'false');
		}).on('hide.bs.dropdown', function () {
			var $menu = $(this).find('.dropdown-menu');
			$menu.first().stop(true, true).slideUp('fast');
			$menu.attr('aria-hidden', 'true');
		});

		$('.collapseContent').on('show.bs.collapse hide.bs.collapse',
			function (event) {
				var ariaHidden = 'true' === $(event.target).attr('aria-hidden').toLowerCase();
				$(event.target).attr('aria-hidden', !ariaHidden );
				$(event.target).parent().find('[data-toggle="collapse"]').toggleClass('open');
			}
		);

		$('.toggle-nav').on('click', function (event) {
			event.preventDefault();
			var $nav = $(event.target).closest('nav').find('> ul');
			if ($nav.is(':visible')) {
				$nav.stop(true, true).slideUp('fast', function () {
					console.log('up donw', this);
					$(this).css('display', '');
				});
			} else {
				$nav.stop(true, true).slideDown('fast');
			}
		});

	});

}(window, document, jQuery));


